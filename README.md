## Pizza Sales (Örnek Tahmin Proje)

Python 3.7 dilinde yazılmış örnek satış verilerini alarak satış tahminleri yapan bir projedir.

### Program Kullanımı

Eğer makinenizde Python derleyici kurulu değilse, ilk adım bunu kurmak olmalıdır. Derleyici kurulduktan sonra projeyi başlatabilirsiniz.

İlk olarak programa uygun formatta hazırlanmış csv dosyanızı tanıtınız.

Programı başlattıktan sonra karşınıza dosyada bulunan ve verilerini görmek istediğiniz şirket isimleri çıkacaktır.

Şirketlerden birini seçtikten sonra şirketin sadece tahmin grafiğini görebilirsiniz hemen alt tarafında ise orjinal verilerin ve tahmin verilerinin beraber olduğu grafiği görüntüleyebilirsiniz.


