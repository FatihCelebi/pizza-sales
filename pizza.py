from flask import Flask,render_template
import numpy as np
import pandas as pd
from datetime import datetime
from sklearn.linear_model import LinearRegression
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from matplotlib.pyplot import figure

def datespec(g):
    DTplot=[]
    
    uzg=len(g)
    i=0
    for i in range(uzg):
        zmn=datetime.strptime(g[i],"%d/%m/%Y %H:%M:%S")
        a=zmn.strftime("%d/%m/%Y")
        DTplot.append(a)
    return DTplot
    
def datatime(c):
    DT=[]
    
    uzc=len(c)
    
    i=0
    for i in range(uzc):
        zmn=datetime.strptime(c[i],"%d/%m/%Y %H:%M:%S")
        k=datetime.timestamp(zmn)
        
        DT.append(k)
    
    return DT  

def tahmin(dx,dy):
    
    x = dx.reshape(-1, 1)
    y= dy.reshape(-1, 1)
    X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.33, random_state=0)
    
   
    #print("Xtrainönce " ,X_train)
    #print("tyyyyönce" ,X_test)
    X_train= X_train.reshape(-1, 1)
    y_train= y_train.reshape(-1, 1)
    X_test = X_test.reshape(-1, 1)
    
    #print("Xtrainsonra",X_train)
    #print("tyyyysonra",X_test)
    
    lr = LinearRegression()
    #print("sorun yokkkkk")
    lr.fit(X_train,y_train)
    
    sonuc = lr.predict(x)
    print("\n\n\n SONUCCC-----\n",sonuc)
    plt.plot(sonuc)
    plt.title("Pizza Sales Tahmin Verileri",fontsize=16)
    return sonuc




        
veri= pd.read_csv("data.csv",encoding="Latin-1")


sirket = input("(Neapolitan Pizza  : 1)\n (Sicilian Pizza   : 2)\n (Tomato Pie Pizza : 3)\n (Fatih Pizza   : 4)\n (ChicagoÂ Pizza :  5 )\n Lütfen görmek istediğiniz şirketin numarasını giriniz :")

         




if str(sirket)=="1":
    
    neo = veri.loc[veri["CATEGORY"]=="Neapolitan Pizza"]
    neo['SALES'].interpolate(method='linear', inplace=True) #****boş verilerin yerine lineer ortalama alarak dolurur.
    
   
    nx=neo.iloc[:,0].values
    #+print(x)
    ny=neo.iloc[:,1].values
    print(ny)
    
    
    Dtplot=datespec(nx)
    
    DT=datatime(nx)
   
  
    
    
    DT=np.array(DT)
    
    z=tahmin(DT,ny)
    
    figure(num=None, figsize=(38, 8))
    plt.plot(Dtplot,ny, label='Neapolitan Pizza',color='m')
    plt.plot(Dtplot,z, label='tahmin',color='k')
    


elif str(sirket)=="2":
    sicil = veri.loc[veri["CATEGORY"]=="Sicilian Pizza"]
    #print("chicag: ",sicil)
    sicil['SALES'].interpolate(method='linear', inplace=True)         #****boş verilerin yerine lineer ortalama alarak dolurur.
    
    sx=sicil.iloc[:,0].values
    print(sx)
    sy=sicil.iloc[:,1].values
    
    print(sy)
    
    Dtplot=datespec(sx)
    
    DT=datatime(sx)
   
    

    
    DT=np.array(DT)
    
    z=tahmin(DT,sy)
   

    
    figure(num=None, figsize=(38, 8))
    
    plt.plot(Dtplot,sy, label='Sicilian Pizza',color='y')
    plt.plot(Dtplot,z, label='tahmin',color='k')
    
    

elif str(sirket)=="3":
    tomato = veri.loc[veri["CATEGORY"]=="Tomato Pie Pizza"]
    tomato['SALES'].interpolate(method='linear', inplace=True)  #****boş verilerin yerine lineer ortalama alarak dolurur.
    print("tomato: ",tomato['SALES'])
    
    
    tx=tomato.iloc[:,0].values
    #print(sx)
    ty=tomato.iloc[:,1].values
    
    
    Dtplot=datespec(tx)
    
    DT=datatime(tx)
  
    
   
    
    DT=np.array(DT)
    
    z=tahmin(DT,ty)
    
    
    #print(sy)
    figure(num=None, figsize=(38, 8))
    plt.plot(Dtplot,ty, label='Tomato Pie Pizza',color='r')
    plt.plot(Dtplot,z, label='tahmin',color='k')
    

elif str(sirket)=="4":
    fth = veri.loc[veri["CATEGORY"]=="Fatih Pizza"]
    fth['SALES'].interpolate(method='linear', inplace=True)  #****boş verilerin yerine lineer ortalama alarak dolurur.
    fx=fth['DATE'].values
    
    fy=fth.iloc[:,1].values
    
    print(fy)
    
    Dtplot=datespec(fx)
    
    DT=datatime(fx)
    
    DT=np.array(DT)
    
    z=tahmin(DT,fy)
    
    #plt(figsize=(15,6))
    
    figure(num=None, figsize=(15, 6))
    
    plt.plot(Dtplot,z, label='tahmin',color='k')
    plt.plot(Dtplot,fy, label='Orjinal',color='b')

elif str(sirket)=="5":
    chi = veri.loc[veri["CATEGORY"]=="ChicagoÂ Pizza"]
    print("chicag: ",chi)
    chi['SALES'].interpolate(method='linear', inplace=True)         #****boş verilerin yerine lineer ortalama alarak dolurur.
    
    cx=chi.iloc[:,0].values
    print(sx)
    cy=chi.iloc[:,1].values
    
    print(cy)
    
    Dtplot=datespec(cx)
    
    DT=datatime(cx)
   
    

    
    DT=np.array(DT)
    
    z=tahmin(DT,cy)
   

    
    figure(num=None, figsize=(38, 8))
    
    plt.plot(Dtplot,cy, label='Sicilian Pizza',color='y')
    plt.plot(Dtplot,z, label='tahmin',color='k')    

plt.title("Pizza Sales",fontsize=16)
plt.xlabel('---TARİH---',fontsize=16)
plt.ylabel('SATIŞ',fontsize=16)
plt.legend()
plt.show()
